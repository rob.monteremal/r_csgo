## R Project: CSGO Pistol Round analysis ##

#### Links ####
 - Git repository: https://gitlab.com/rob.monteremal/r_csgo
 - Full dataset *(kaggle)*: https://www.kaggle.com/skihikingkevin/csgo-matchmaking-damage

#### Instructions ####
 - *notebook.ipynb* and *notebook.r* contain the same R scripts
 - Preferably you should use *notebook.ipynb*: it is a Jupyter Notebook file which is easier to read (markdown cells) and to execute
 - To execute *notebook.ipynb* you need Jupyter Notebook installed with the R kernel