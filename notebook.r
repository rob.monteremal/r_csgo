
# Installs (only do it once per computer)
# install.packages('dplyr')
# install.packages('ggplot2')

# Imports
library('plyr')
library('dplyr')
library('ggplot2')

# Opening a file from the dataset (first 5 rows for exploration)
filename = "dataset/esea_meta_demos.part1.csv"
df = read.csv(filename, header=TRUE, sep=",", nrows = 5)

# Print its header and content
head(df)

# Matchmaking demos
# filename = "dataset/mm_master_demos.csv"
# df_mm = read.csv(filename, header=TRUE, sep=",", nrows = -1)

# Esea demos meta files
filename = "dataset/esea_meta_demos.part1.csv"
df1 = read.csv(filename, header=TRUE, sep=",")
filename = "dataset/esea_meta_demos.part2.csv"
df2 = read.csv(filename, header=TRUE, sep=",")

# Append both dataframes
df_esea = rbind(df1, df2)

#df = df_mm
df = df_esea

# Remove duplicate data, going from one entry per hit to one entry per game
df_duplicates = df[c("file")]
new_df = df[!duplicated(df_duplicates), ]
map_distrib = count(new_df, map)
map_distrib = mutate(map_distrib, n = n/sum(n))

bp <- ggplot(map_distrib, aes(x="", y=n, fill=map)) +
  geom_bar(width = 1, stat = "identity") +
  coord_polar("y", start=0) +
  scale_x_discrete(name="") +
  scale_y_continuous(name="Map distribution", breaks=seq(0,0.95,0.05))
bp

# Remove duplicate data, going from one entry per hit to one entry per round
df_duplicates = df[c("file", "round")]
new_df = df[!duplicated(df_duplicates), ]

# Extracting only relevant data
new_df = new_df[,c("file", "round", "winner_side", "map") ]

# Count winning side distribution on each map
df_count = count(new_df, map, winner_side)

# Remove "None" data (draw should be impossible..)
df_count = df_count[(which(df_count$winner_side != "None")),]

# Plot the data
ggplot(df_count, aes(fill=winner_side, y=n, x=map)) +
  geom_bar(position="fill", stat="identity") +
  theme(axis.text.x = element_text(angle=45, hjust=1)) +
  scale_fill_manual("Winner side", values = c("CounterTerrorist" = "cornflowerblue", "Terrorist" = "burlywood2")) +
  scale_x_discrete(name="Map") +
  scale_y_continuous(name="Round wins distribution", breaks=seq(0,1,0.1)) +
  geom_abline(slope=0, intercept=0.5,  col="red", lty=2)

# Remove duplicate data, going from one entry per hit to one entry per round
df_duplicates = df[c("file", "round")]
new_df = df[!duplicated(df_duplicates), ]

# Extracting only relevant data
new_df = new_df[,c("file", "round", "winner_side", "map", "winner_team", "round_type") ]

# Grouping data by match
grp_df = group_by(new_df, file)
nrow(grp_df)

# Each match where the data on either pistol round is unavailable is removed (~17%)
grp_df = filter(grp_df, sum(round %in% c(1,16)) == 2)
# Remove matches where a round is missing (~20%)
grp_df = filter(grp_df, n() == max(round))
# Overtime data is removed (match is considered as ending at 15-15)
grp_df = grp_df[which(grp_df$round <= 30),]
# Remove matches where no one won (data collection ended before the game), either a team won with 16 rounds or the game ended 15-15
grp_df = filter(grp_df, sum(winner_team == "Team 1") == 16 | sum(winner_team == "Team 2") == 16 | (sum(winner_team == "Team 2") == 15 & sum(winner_team == "Team 1") == 15) )
# Round marked as PISTOL_ROUND while it isn't either the 1st or 16th round
grp_df = filter(grp_df, !(!(round %in% c(1,16)) & round_type=="PISTOL_ROUND"))
# Add half data
grp_df = mutate(grp_df, half = (round<=15) + 2 * (round>15))

# Wins are counted for each team on each side
ct_win_df = count(grp_df[which(grp_df$winner_side == "CounterTerrorist"),], half)
t_win_df = count(grp_df[which(grp_df$winner_side == "Terrorist"),], half)
# Rename counting variables (default name is "n")
names(ct_win_df)[names(ct_win_df) == "n"] = "ct_wins"
names(t_win_df)[names(t_win_df) == "n"] = "t_wins"
# Merge the 2 dataframes to have both CT and T wins on each side, with 0 when a team hasn't won a single round in a side
win_df = merge(ct_win_df[,c("file","half", "ct_wins")], t_win_df[,c("file","half", "t_wins")], all = TRUE)
win_df[is.na(win_df)] = 0

# Extracts the winner each pistol round and renames the column
pistol_round_df = filter(grp_df, round==1 | round==16)[,c("file", "half", "winner_side", "map")]
names(pistol_round_df)[names(pistol_round_df) == "winner_side"] = "pistol_winner"
# Merge the 2 dataframes
final_df = merge(win_df, pistol_round_df)

# Counting the data
ct_win = cbind(half_winner="CounterTerrorist", count(filter(final_df, ct_wins > t_wins), pistol_winner))
t_win = cbind(half_winner="Terrorist", count(filter(final_df, ct_wins < t_wins), pistol_winner))
tie = cbind(half_winner="Tie", count(filter(final_df, ct_wins == t_wins), pistol_winner))

count_df = group_by(rbind(ct_win,tie,t_win), pistol_winner)

# Plotting the data
ggplot(count_df, aes(fill=half_winner, y=n, x=pistol_winner)) +
  geom_bar(position="fill", stat="identity") +
  scale_fill_manual(values = c("CounterTerrorist" = "cornflowerblue","Tie" = "gray", "Terrorist" = "burlywood2")) +
  scale_x_discrete(name="Winner of the pistol round") +
  scale_y_continuous(name="Probability of winning the half" ,breaks=seq(0,1,0.1)) +
  geom_abline(slope=0, intercept=0.5,  col="red", lty=2)

count_df2 = mutate(count_df, half_winning_probability = n/sum(n))

# Plotting the data
ggplot(count_df2, aes(fill=half_winner, y=half_winning_probability, x=pistol_winner)) +
  geom_bar(position="dodge", stat="identity") +
  scale_fill_manual(values = c("CounterTerrorist" = "cornflowerblue","Tie" = "gray", "Terrorist" = "burlywood2")) +
  scale_x_discrete(name="Winner of the pistol round") +
  scale_y_continuous(name="Probability of winning the half" ,breaks=seq(0,1,0.1)) +
  geom_abline(slope=0, intercept=0.5,  col="red", lty=2)

# Each match where the data on either pistol round is unavailable is removed (~17%)
grp_df = filter(grp_df, sum(round %in% c(1,16)) == 2)
# Overtime data is removed (match is considered as ending at 15-15)
grp_df = grp_df[which(grp_df$round <= 30),]
# Remove matches where a round is missing (~12%)
grp_df = filter(grp_df, n() == max(round))
# Round marked as PISTOL_ROUND while it isn't either the 1st or 16th round
grp_df = filter(grp_df, !(!(round %in% c(1,16)) & round_type=="PISTOL_ROUND"))
# Add half data
grp_df = mutate(grp_df, half = (round<=15) + 2 * (round>15))

# Wins are counted for each team on each side
ct_win_df = count(grp_df[which(grp_df$winner_side == "CounterTerrorist"),], half)
t_win_df = count(grp_df[which(grp_df$winner_side == "Terrorist"),], half)
# Rename counting variables (default name is "n")
names(ct_win_df)[names(ct_win_df) == "n"] = "ct_wins"
names(t_win_df)[names(t_win_df) == "n"] = "t_wins"
# Merge the 2 dataframes to have both CT and T wins on each side, with 0 when a team hasn't won a single round in a side
win_df = merge(ct_win_df[,c("file","half", "ct_wins")], t_win_df[,c("file","half", "t_wins")], all = TRUE)
win_df[is.na(win_df)] = 0

# Extracts the winner each pistol round and renames the column
pistol_round_df = filter(grp_df, round==1 | round==16)[,c("file", "half", "winner_side", "map")]
names(pistol_round_df)[names(pistol_round_df) == "winner_side"] = "pistol_winner"
# Merge the 2 dataframes
final_df = merge(win_df, pistol_round_df)

# Counting the data
ct_win = cbind(half_winner="CounterTerrorist", count(filter(final_df, ct_wins > t_wins), pistol_winner, map))
t_win = cbind(half_winner="Terrorist", count(filter(final_df, ct_wins < t_wins), pistol_winner, map))
count_df = group_by(rbind(ct_win,t_win), pistol_winner)

# Extracts the winner each pistol round and renames the column
pistol_round_df = filter(grp_df, round==1 | round==16)[,c("file", "half", "winner_side", "map")]
names(pistol_round_df)[names(pistol_round_df) == "winner_side"] = "pistol_winner"

# Evaluates if the pistol round winner won the half or not and adds the result as a column 
count_same_df = mutate(count_df, same = (as.character(pistol_winner) == as.character(half_winner) ) )[,c("same", "map", "n")]

# Plotting the data
ggplot(count_same_df, aes(fill=same, y=n, x=map) ) +
  geom_bar(position="fill", stat="identity") +
  theme(axis.text.x = element_text(angle=45, hjust=1)) +
  scale_fill_discrete("Winner of the pistol round won the half") +
  scale_x_discrete(name="Map") +
  scale_y_continuous(name="Probability of winning the half when winning the pistol round", breaks=seq(0,1,0.1)) +
  geom_abline(slope=0, intercept=0.5,  col="red", lty=2)

# Counting the number of round won per team
raw_df = mutate(grp_df, t1_wins=sum(winner_team == "Team 1"), t2_wins=sum(winner_team == "Team 2"))[, c("file", "map", "t1_wins", "t2_wins")]
team_wins_df = raw_df[!duplicated(raw_df$file),]

# Extracts the winner each pistol round and renames the column
pistol_round_df = filter(grp_df, round==1 | round==16)[,c("file", "half", "winner_team", "map")]
names(pistol_round_df)[names(pistol_round_df) == "winner_team"] = "pistol_winner"

# Merging with the pistol data
final_df = merge(team_wins_df, pistol_round_df)

# Marking the data with the proper game winner
t1_win = cbind(game_winner="Team 1", filter(final_df, t1_wins == 16))[,c("file", "map", "game_winner", "half", "pistol_winner")]
t2_win = cbind(game_winner="Team 2", filter(final_df, t2_wins == 16))[,c("file", "map", "game_winner", "half", "pistol_winner")]
merged_df = rbind(t1_win, t2_win)

# Count the data
merged_df = mutate(group_by(merged_df, file), nb_pistol_won = sum(as.character(pistol_winner) == as.character(game_winner)))
count_df = count(group_by(ungroup(merged_df), map), map, nb_pistol_won)

# Calculating the probability of winning a game by winning x number of pistol rounds
# Remember: for each game won with 1 pistol round, there is a game lost with 1 pistol round (opposite team)
#     for each game won with 2 pistol rounds, there is a game lost with 0 pistol round (opposite team)
#     for each game won with 0 pistol round, there is a game lost with 2 pistol rounds (opposite team)
count_df2 = mutate(filter(count_df, nb_pistol_won == 0 | nb_pistol_won == 2), n = n/sum(n))
count_df1 = mutate(filter(count_df, nb_pistol_won == 1), n = n/(2*sum(n)) )
count_df = rbind(count_df1, count_df2)

# Plotting the data
ggplot(count_df, aes(fill=factor(nb_pistol_won), y=n, x=map) ) +
  geom_bar(position=position_dodge(), stat="identity") +
  theme(axis.text.x = element_text(angle=45, hjust=1)) +
  scale_fill_discrete("Number of pistol rounds won") +
  scale_x_discrete(name="Map") +
  scale_y_continuous(name="Probabily of winning the game by winning x pistol rounds", breaks=seq(0,1,0.1)) +
  geom_abline(slope=0, intercept=0.5,  col="red", lty=2)
